import React, { Component, Fragment } from 'react'

export default class Form extends Component {

  constructor() {
    super();
    this.state = {
      showValidation: false
    }
  }

  getNewTask = (event) => {
    event.preventDefault()
    let form = event.target;
    let taskName = form.taskName.value;
    taskName ? this.props.task(taskName) :
      this.setState({
        showValidation: true
      });
  }

  render() {
    return (
      <Fragment>
        <form className="form" onSubmit={this.getNewTask} >
          <input className="task__name" name="taskName" type="text" placeholder="Task" />
          <input className="task__save" type="submit" value="Save" />
        </form>
        {this.state.showValidation && <p className="error">Input should not be empty.</p>}
      </Fragment>
    );
  };
};
