import React, { Component } from 'react'
import Task from '../task/Task';

export default class Tasks extends Component {

  mapTasks() {
    return this.props.data
      .map(task => <Task task={task} key={task.id} taskID={this.props.taskID} />);
  }

  render() {
    return (
      <ul className="taskdeck">
        {this.mapTasks()}
      </ul>
    );
  }
}