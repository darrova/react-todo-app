import React, { Component } from 'react';

export default class Sidebar extends Component {

  createTask = () => {
    this.props.show(true);
  };

  allTasks = () => {
    this.props.filter(true, true)
  }

  completed = () => {
    this.props.filter(true)
  }

  uncompleted = () => {
    this.props.filter(false)
  }

  render() {
    return (
      <aside className="sidebar">
        <ul className="menu">
          <li className="menu__option">
            <input className="search-box" type="search" placeholder="Search" />
          </li>
          <li className="menu__option menu--selected">
            <button onClick={this.allTasks}>All Tasks</button>
          </li>
          <li className="menu__option">
            <button onClick={this.completed}>Complete</button>
          </li>
          <li className="menu__option">
            <button onClick={this.uncompleted}>Incomplete</button>
          </li>
          <li className="menu__option menu__new-task">
            <button onClick={this.createTask}>+ New Task</button>
          </li>
        </ul>
      </aside>
    )
  };
}