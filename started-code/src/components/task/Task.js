import React, { Component } from 'react'

export default class Task extends Component {

  isCompleted = (value) => {
    this.props.taskID(value);
  }

  render() {
    const { id, name, completed } = this.props.task;
    return (
      <li className={completed ? 'taskcard taskcard--completed' : 'taskcard'}>
        <input type="checkbox" name="isCompleted" onChange={() => this.isCompleted(id)} />
        <span>{name}</span>
      </li>
    );
  }
}