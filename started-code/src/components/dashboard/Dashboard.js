import React, { Component, Fragment } from 'react'
import Form from '../form/Form';
import Tasks from '../tasks/Tasks';

export default class Dashboard extends Component {

  showDate() {
    return new Date().toDateString();
  };

  addTask = (value) => {
    this.props.task(value)
  }

  render() {
    return (
      <Fragment>
        {this.props.show && <Form task={this.addTask} />}
        <h1>Today</h1>
        <time>{this.showDate()}</time>
        <hr />
        <h2>Tasks</h2>
        <Tasks
          data={this.props.tasks}
          taskID={this.props.taskID} />
      </Fragment>
    );
  };
};