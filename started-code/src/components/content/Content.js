import React, { Component } from 'react'
import Dashboard from '../dashboard/Dashboard';

export default class Content extends Component {
  render() {
    return (
      <main className="body">
        <Dashboard
          tasks={this.props.data}
          task={this.props.newTask}
          show={this.props.showForm}
          taskID={this.props.taskID}
        />
      </main>

    );
  };
}