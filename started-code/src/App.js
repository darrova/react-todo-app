import React, { Component } from 'react';

import "./App.css";
import Sidebar from './components/sidebar/Sidebar';
import Content from './components/content/Content';

class App extends Component {

  constructor() {
    super();
    this.state = {
      tasks: [],
      showForm: false,
      filtered: []
    }
  }

  addToTasks(task) {
    this.setState({
      tasks: [...this.state.tasks, task],
      filtered: [...this.state.filtered, task],
      showForm: false,
    });
  }

  createTask = (value) => {
    let newTask = {
      id: this.state.tasks.length + 1,
      name: value,
      completed: false,
    }
    this.addToTasks(newTask);
  }

  showForm = () => {
    this.setState({
      showForm: true
    })
  }

  isCompleted = (id) => {
    const index = this.state.tasks.findIndex(task => task.id === id);
    const newTasks = this.state.tasks;
    newTasks[index].completed = !newTasks[index].completed;
    this.setState({
      tasks: newTasks
    });
  }

  filterTasks = (completed, all) => {
    if (all === undefined) {
      const filteredTasks = this.state.tasks.filter(task => task.completed === completed);
      this.setState({
        filtered: filteredTasks
      });
    } else {
      this.setState({
        filtered: this.state.tasks
      })
    }
  }

  render() {
    return (
      <div className="container">
        <Sidebar
          show={this.showForm}
          filter={this.filterTasks}
        />
        <Content
          data={this.state.filtered}
          newTask={this.createTask}
          showForm={this.state.showForm}
          taskID={this.isCompleted} />
      </div>
    );
  }
}

export default App;
